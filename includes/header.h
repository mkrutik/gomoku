#ifndef HEADER_H
#define HEADER_H

#include <vector>
#include <array>
#include <iostream>


#define MAX_SIZE	19
#define MAX_VIEW_LINES ((MAX_SIZE * 2) - 1)

#define INFO	"INFO"
#define WARNING	"WARNING"
#define ERROR	"ERROR"

#ifdef DEBUG
	#include <thread>
	#include <chrono>
	#include <iomanip>

	#define LOG(type, msg) \
	{																						\
		auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); 	\
		std::cout << "[ " << type << " ]" 													\
			<< " [ " << std::put_time(std::localtime(&t), "%H:%M:%S  %Y-%m-%d") << " ]" 	\
			<< " [ " << std::this_thread::get_id() << " ]"									\
			<< " [ " << __func__ << " ]" 													\
			<< " [ " << msg << " ]" << std::endl;											\
	}
#else
	#define LOG(type, msg) {}
#endif


typedef struct				s_rules {
	bool					enable_capture;
	bool					forbid_double_threes;
	bool					move_hint;
	bool 					only_five;
} 							t_rules;

typedef enum 				e_state : signed char {
	SYS_INFO				= -1,
	X						= 0,
	O						= 1,
	FREE					= 2
}							t_state;

typedef struct				s_board_inf {
	t_state					horizontal[MAX_SIZE][MAX_SIZE];
	t_state					vertical[MAX_SIZE][MAX_SIZE];
	t_state					ld_ru[MAX_VIEW_LINES][MAX_SIZE];
	t_state					lu_rd[MAX_VIEW_LINES][MAX_SIZE];

	int 					size;
}							t_board;

typedef struct				s_point {
	int						x;
	int						y;
}							t_point;

typedef struct				s_heuristic {
	int						five;

	int						full_open_one;
	int						full_open_two;
	int						full_open_three;
	int						full_open_four;

	int						open_one;
	int						open_two;
	int						open_three;
	int						open_four;

	int						n_capture;
	long long				all_sum;

	s_heuristic& operator += (const s_heuristic &left)
	{
		this->five += left.five;
		this->full_open_one += left.full_open_one;
		this->full_open_two += left.full_open_two;
		this->full_open_three += left.full_open_three;
		this->full_open_four += left.full_open_four;
		this->open_one += left.open_one;
		this->open_two += left.open_two;
		this->open_three += left.open_three;
		this->open_four += left.open_four;
		this->n_capture += left.n_capture;
		this->all_sum += left.all_sum;
		return *this;
	}
}							t_heuristic;


typedef struct				s_views_heuristic {
	t_heuristic				heu_horizontal[MAX_SIZE];
	t_heuristic				heu_vertical[MAX_SIZE];
	t_heuristic				heu_ld_ru[MAX_VIEW_LINES];
	t_heuristic				heu_lu_rd[MAX_VIEW_LINES];
	t_heuristic				all;
}							t_views_heuristic;

typedef struct				s_board_heur {
	t_board					board;

	t_views_heuristic		heu[2];
}							t_board_heur;


typedef struct 				s_captured_array {
	t_point					arr[16];
	int						size;
}							t_captured_array;

typedef struct				s_move {
	t_point					move_cord;
	t_captured_array		captured;
	t_heuristic				heuristic;
}							t_move;

#define BEST_MOVES_AM 3

typedef struct
{
	t_move					moves[BEST_MOVES_AM];
	t_board_heur			boards[BEST_MOVES_AM];
	size_t					size;
}							t_best_moves;

typedef struct 				s_move_array {
	t_move					arr[MAX_SIZE * MAX_SIZE];
	int						size;
}							t_move_array;

// Human = true, computer = false
typedef struct				s_player {
	bool					human;
	t_state					figure;
	uint8_t					captured;
	const char				*name;
}							t_player;

typedef struct				s_min_max_inf {
	int						cap[2];

	int						depth;
	bool					maximizing;

	long long				alpha;
	long long				beta;
}							t_min_max_inf;

typedef struct				s_view_indexes {
	int						row;
	int 					cell;
}							t_view_indexes;

#endif