#ifndef GUI_H
#define GUI_H

#include <string>

const unsigned int WIN_WIDHT = 800;
const unsigned int WIN_HEIGHT = 600;


const std::string START = "START";
const std::string EXIT = "EXIT";
const std::string OPTIONS = "OPTIONS";
const std::string RETURN = "RETURN";

const std::string PLAYER_1 = "PLAYER 1";
const std::string PLAYER_2 = "PLAYER 2";
const char *HUMAN = "Human";
const char *COMPUTER = "Computer";
const std::string CAPTURED = "Captured";
const std::string DOUBLE_THREE = "Forbide double-three";
const std::string CAPTURING = "Enable capturing";
const std::string ONLY_FIVE_WINS = "Enable only five wins";
const std::string BOARD_SIZE = "Board size";
const std::string SHOW_HINT = "Show hint";

const std::string WINNER_TEXT = "WINNER";
const std::string DRAW_GAME = "DRAW GAME";

const uint32_t BACKGROUND_COLOR = 0x424949;

/* Font */
const std::string FONT_PATH = "../resource/font/shanghai.ttf";
const std::string TIME_FONT = "../resource/font/FONT.ttf";

/* Imgs */
const std::string BACKGROUND_IMG_PATH = "../resource/img/background.jpg";
const std::string BLACK_STONE_PATH = "../resource/img/black_stone.png";
const std::string WHITE_STONE_PATH = "../resource/img/white_stone.png";
const std::string HINT_PATH = "../resource/img/hint.png";
const std::string CHECKBOX_UNCHECKED = "../resource/img/checkbox_unchecked.png";
const std::string CHECKBOX_CHECKED = "../resource/img/checkbox_checked.png";
const std::string MCHECKBOX_UNCHECKED = "../resource/img/multi_check_unchecked.png";
const std::string MCHECKBOX_CHECKED = "../resource/img/multi_check_checked.png";
const std::string VOLUME_ON = "../resource/img/volume_on.png";
const std::string VOLUME_OFF = "../resource/img/volume_off.png";
const std::string CELL = "../resource/img/grid.png";
const std::string WATCH = "../resource/img/watch.png";

/* Music */
const std::string MUSIC_PATH = "../resource/music/Little Dragon.wav";

#endif