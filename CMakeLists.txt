cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

############# Compiler config
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3 -Wall -Wextra -Werror")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++11 -Wall -Wextra -Werror")
########################################

set(CMAKE_VERBOSE_MAKEFILE ON)

project(Gomoku CXX)

option(DEBUG "Option description" OFF)
if(DEBUG)
    add_definitions(-DDEBUG)
endif(DEBUG)


############# looking for required libs
link_directories(/usr/local/lib)
find_package(Threads REQUIRED)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake_modules")
find_package(PkgConfig REQUIRED)
pkg_check_modules(SFML REQUIRED sfml-all)
#find_package(SFML REQUIRED system window graphics network audio)
########################################


############# Includes
include_directories(${SFML_INCLUDE_DIRS})

list (APPEND PROJECT_HEADERS
	includes/header.h
	includes/GUI.h

	src/Game.hpp
	src/logic/Logic.hpp
)
########################################


############# Sources
list (APPEND PROJECT_SOURCES
	src/main.cpp

	src/Game.cpp

	src/logic/Logic.cpp
	src/logic/capturing.cpp
	src/logic/move_generator.cpp
	src/logic/min_max_algorithm.cpp
	src/logic/estimation.cpp
	src/logic/view_helpers.cpp
)
########################################


############# Compiling
add_executable(${PROJECT_NAME}
	${PROJECT_SOURCES}
	${PROJECT_HEADERS}
)
########################################


############# Linking
target_link_libraries(${PROJECT_NAME}
	${SFML_LIBRARIES}
	${CMAKE_THREAD_LIBS_INIT}
)
########################################

# ERROR: Package 'openal', required by 'sfml-audio', not found
# export PKG_CONFIG_PATH="/Users/mkrutik/.brew/opt/openal-soft/lib/pkgconfig"