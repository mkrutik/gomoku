#ifndef GAME_HPP
#define GAME_HPP

#include "../includes/header.h"

class Game {
public:
	void	start_game(void);

private:
	void	show_menu(void);
	void	playing_loop(void);

	t_board		_board;
	t_rules		_rules;
	t_player	_players[2];
};

#endif