#include "Game.hpp"

#include "../includes/GUI.h"
#include "logic/Logic.hpp"

#include <chrono>
#include <iomanip>
#include <sstream>
#include <map>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

namespace
{
sf::RenderWindow window(sf::VideoMode(WIN_WIDHT, WIN_HEIGHT), L"Gomoku", sf::Style::Close);
sf::Music music;
sf::Font font;
sf::Font time_font;
sf::Color text_color(255, 178, 0);
sf::Color background_color_grey(25, 25, 25, 180);
sf::Color background_color_green(0, 128, 0, 170);

sf::Texture bacground;
sf::Texture checkbox_checked;
sf::Texture checkbox_unchecked;
sf::Texture m_checkbox_checked;
sf::Texture m_checkbox_unchecked;
sf::Texture bs_texture;
sf::Texture ws_texture;
sf::Texture hint_texture;
sf::Texture volume_on;
sf::Texture volume_off;
sf::Texture cell;
sf::Texture watch;

unsigned int win_width = WIN_WIDHT;
unsigned int win_height = win_height;

unsigned int desctop_wight = 0;
unsigned int desctop_height = 0;

#ifdef __APPLE__
	uint8_t	d_scale = 2;
#else
	uint8_t	d_scale = 1;
#endif

Logic *logic;

void initialize_grid(sf::Sprite *grid, int board_size, int cell_size, const int x_pos, const int y_pos)
{
	const float scale = (float)cell_size / cell.getSize().x;

	for (int y = 0; y < board_size; ++y)
		for (int x = 0; x < board_size; ++x)
		{
			grid[y * board_size + x].setTexture(cell);
			grid[y * board_size + x].setScale(scale, scale);
			grid[y * board_size + x].setPosition(x_pos + x * cell_size, y_pos + y * cell_size);
		}
}

void draw_board(sf::Sprite *grid, int board_size)
{
	for (int y = 0; y < board_size; ++y)
		for (int x = 0; x < board_size; ++x)
			window.draw(grid[y * board_size + x]);
}

std::string get_duration(std::chrono::nanoseconds ns)
{
	auto m = std::chrono::duration_cast<std::chrono::minutes>(ns);
	ns -= m;
	auto s = std::chrono::duration_cast<std::chrono::seconds>(ns);
	ns -= s;
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(ns);
	ns -= ms;
	while (ms > std::chrono::milliseconds(500))
		ms -= std::chrono::milliseconds(100);
	auto mc = std::chrono::duration_cast<std::chrono::microseconds>(ns);

	std::stringstream ss;
	char fill = ss.fill();
	ss.fill('0');

	ss << std::setw(2) << m.count() << "m: "
	   << std::setw(2) << s.count() << "s: "
	   << std::setw(2) << ms.count() << "ms: "
	   << std::setw(2) << mc.count() << "mc";

	ss.fill(fill);

	return ss.str();
};

} // namespace

void initialization(void)
{
	auto desktop = sf::VideoMode::getDesktopMode();
	desctop_wight = desktop.width;
	desctop_height = desktop.height;

	window.setFramerateLimit(60);

	/* ------------------------------------------- */
	music.openFromFile(MUSIC_PATH);
	music.setVolume(50);
	music.setLoop(true);
	/* ------------------------------------------- */

	if (!font.loadFromFile(FONT_PATH))
	{
		LOG(ERROR, "Can't load font file : " + FONT_PATH)
		exit(1);
	}

	if (!time_font.loadFromFile(TIME_FONT))
	{
		LOG(ERROR, "Can't load font file : " + TIME_FONT)
		exit(1);
	}

	if (!bacground.loadFromFile(BACKGROUND_IMG_PATH))
	{
		LOG(ERROR, "Can't load texture file : " + BACKGROUND_IMG_PATH)
		exit(1);
	}

	if (!checkbox_checked.loadFromFile(CHECKBOX_CHECKED))
	{
		LOG(ERROR, "Can't load texture file : " + CHECKBOX_CHECKED)
		exit(1);
	}

	if (!checkbox_unchecked.loadFromFile(CHECKBOX_UNCHECKED))
	{
		LOG(ERROR, "Can't load texture file : " + CHECKBOX_UNCHECKED)
		exit(1);
	}

	if (!m_checkbox_checked.loadFromFile(MCHECKBOX_CHECKED))
	{
		LOG(ERROR, "Can't load texture file : " + MCHECKBOX_CHECKED)
		exit(1);
	}

	if (!m_checkbox_unchecked.loadFromFile(MCHECKBOX_UNCHECKED))
	{
		LOG(ERROR, "Can't load texture file : " + MCHECKBOX_UNCHECKED)
		exit(1);
	}

	if (!bs_texture.loadFromFile(BLACK_STONE_PATH))
	{
		LOG(ERROR, "Can't load texture file : " + BLACK_STONE_PATH)
		exit(1);
	}

	if (!ws_texture.loadFromFile(WHITE_STONE_PATH))
	{
		LOG(ERROR, "Can't load texture file : " + WHITE_STONE_PATH)
		exit(1);
	}

	if (!volume_on.loadFromFile(VOLUME_ON))
	{
		LOG(ERROR, "Can't load texture file : " + VOLUME_ON)
		exit(1);
	}

	if (!volume_off.loadFromFile(VOLUME_OFF))
	{
		LOG(ERROR, "Can't load texture file : " + VOLUME_OFF)
		exit(1);
	}

	if (!cell.loadFromFile(CELL))
	{
		LOG(ERROR, "Can't load texture file : " + CELL)
		exit(1);
	}

	if (!watch.loadFromFile(WATCH))
	{
		LOG(ERROR, "Can't load texture file : " + WATCH)
		exit(1);
	}

	if (!hint_texture.loadFromFile(HINT_PATH))
	{
		LOG(ERROR, "Can't load texture file : " + HINT_PATH)
		exit(1);
	}
	/* ------------------------------------------- */

	logic = Logic::get_instance();
	music.play();
	LOG(INFO, "media files successfully loaded")
}

void Game::show_menu(void)
{
	_board.size = MAX_SIZE;
	_rules.enable_capture = true;
	_rules.forbid_double_threes = true;
	_rules.only_five = false;
	_rules.move_hint = true;

	// Human = true, computer = false
	_players[0] = {true, X, 0, NULL};
	_players[1] = {false, O, 0, NULL};


	// for menu view used default window size
	win_width = WIN_WIDHT * d_scale;
	win_height = WIN_HEIGHT * d_scale;

	window.setView(sf::View(sf::FloatRect(0, 0, win_width, win_height)));
	window.setSize({win_width, win_height});
	window.setPosition({(int)(desctop_wight / 2 - window.getSize().x / 2), (int)(desctop_height / 2 - window.getSize().y / 2)});

	/******************** IMG background ********************/
	static sf::Sprite sp_bacground;
	sp_bacground.setTexture(bacground);
	sp_bacground.setPosition(0, 0);
	sp_bacground.setScale((double)win_width / bacground.getSize().x, (double)win_height / bacground.getSize().y);

	/* ------------------------------------------- */

	/******************** OPTION text ********************/
	static sf::Text option;
	option.setString(OPTIONS);
	option.setFont(font);
	option.setCharacterSize(24 * d_scale);
	option.setFillColor(sf::Color::Black);
	option.setPosition((win_width / 2) - option.getLocalBounds().width / 2, 20 * d_scale);
	option.setStyle(sf::Text::Underlined | sf::Text::Bold);
	/* ------------------------------------------- */

	/******************** PLAYER 1 menu ********************/
	static sf::RectangleShape p1_background(sf::Vector2f(win_width / 2 - 40, win_height / 3));
	p1_background.setFillColor(background_color_grey);
	p1_background.setPosition(20, 80 * d_scale);

	static sf::Text player_1;
	player_1.setString(PLAYER_1);
	player_1.setFont(font);
	player_1.setCharacterSize(24 * d_scale);
	player_1.setFillColor(text_color);
	player_1.setPosition(50 * d_scale, 90 * d_scale);

	static sf::Sprite black_stone;
	black_stone.setTexture(bs_texture);
	black_stone.setPosition(50 * d_scale + player_1.getLocalBounds().width + 30 * d_scale, 85 * d_scale);
	black_stone.setScale(0.1 * d_scale, 0.1 * d_scale);

	static sf::Text chose_human_1;
	chose_human_1.setString(HUMAN);
	chose_human_1.setFont(font);
	chose_human_1.setCharacterSize(22 * d_scale);
	chose_human_1.setFillColor(text_color);
	chose_human_1.setPosition(70 * d_scale, 150 * d_scale);

	static sf::Sprite checkbox_1;
	checkbox_1.setTexture(checkbox_checked);
	checkbox_1.setPosition(win_width / 2 - 80 * d_scale, 150 * d_scale);
	checkbox_1.setScale(0.15 * d_scale, 0.15 * d_scale);

	static sf::Text chose_computer_1;
	chose_computer_1.setString(COMPUTER);
	chose_computer_1.setFont(font);
	chose_computer_1.setCharacterSize(22 * d_scale);
	chose_computer_1.setFillColor(text_color);
	chose_computer_1.setPosition(70 * d_scale, 200 * d_scale);

	static sf::Sprite checkbox_2;
	checkbox_2.setTexture(checkbox_unchecked);
	checkbox_2.setPosition(win_width / 2 - 80 * d_scale, 200 * d_scale);
	checkbox_2.setScale(0.15 * d_scale, 0.15 * d_scale);
	/* ------------------------------------------- */

	/******************** PLAYER 2 menu ********************/
	static sf::RectangleShape p2_background(sf::Vector2f(win_width / 2 - 40, win_height / 3));
	p2_background.setFillColor(background_color_grey);
	p2_background.setPosition(win_width / 2 + 20, 80 * d_scale);

	static sf::Text player_2;
	player_2.setString(PLAYER_2);
	player_2.setFont(font);
	player_2.setCharacterSize(24 * d_scale);
	player_2.setFillColor(text_color);
	player_2.setPosition(win_width / 2 + 50 * d_scale, 90 * d_scale);

	static sf::Sprite white_stone;
	white_stone.setTexture(ws_texture);
	white_stone.setPosition(win_width / 2 + 50 * d_scale + player_2.getLocalBounds().width + 30 * d_scale, 85 * d_scale);
	white_stone.setScale(0.1 * d_scale, 0.1 * d_scale);

	static sf::Text chose_human_2;
	chose_human_2.setString(HUMAN);
	chose_human_2.setFont(font);
	chose_human_2.setCharacterSize(22 * d_scale);
	chose_human_2.setFillColor(text_color);
	chose_human_2.setPosition(win_width / 2 + 70 * d_scale, 150 * d_scale);

	static sf::Sprite checkbox_3;
	checkbox_3.setTexture(checkbox_unchecked);
	checkbox_3.setPosition(win_width - 80 * d_scale, 150 * d_scale);
	checkbox_3.setScale(0.15 * d_scale, 0.15 * d_scale);

	static sf::Text chose_computer_2;
	chose_computer_2.setString(COMPUTER);
	chose_computer_2.setFont(font);
	chose_computer_2.setCharacterSize(22 * d_scale);
	chose_computer_2.setFillColor(text_color);
	chose_computer_2.setPosition(win_width / 2 + 70 * d_scale, 200 * d_scale);

	static sf::Sprite checkbox_4;
	checkbox_4.setTexture(checkbox_checked);
	checkbox_4.setPosition(win_width - 80 * d_scale, 200 * d_scale);
	checkbox_4.setScale(0.15 * d_scale, 0.15 * d_scale);
	/* ------------------------------------------- */

	/******************** START btn ********************/
	static sf::RectangleShape start_background(sf::Vector2f(100 * d_scale, 40 * d_scale));
	start_background.setFillColor(background_color_green);
	start_background.setPosition(win_width / 2 - 110 * d_scale, win_height - 60 * d_scale);

	static sf::Text start_btn;
	start_btn.setString(START);
	start_btn.setFont(font);
	start_btn.setCharacterSize(24 * d_scale);
	start_btn.setFillColor(sf::Color::White);
	start_btn.setPosition(start_background.getPosition().x + (start_background.getLocalBounds().width - start_btn.getLocalBounds().width) / 2,
						start_background.getPosition().y + (start_background.getLocalBounds().height - start_btn.getLocalBounds().height) / 4);
	/* ------------------------------------------- */

	/******************** EXIT btn ********************/
	static sf::RectangleShape exit_background(sf::Vector2f(100 * d_scale, 40 * d_scale));
	exit_background.setFillColor(sf::Color(255, 0, 0, 170));
	exit_background.setPosition(win_width / 2 + 10 * d_scale, win_height - 60 * d_scale);

	static sf::Text exit_btn;
	exit_btn.setString(EXIT);
	exit_btn.setFont(font);
	exit_btn.setCharacterSize(24 * d_scale);
	exit_btn.setFillColor(sf::Color::White);
	exit_btn.setPosition(exit_background.getPosition().x + (exit_background.getLocalBounds().width - exit_btn.getLocalBounds().width) / 2,
						exit_background.getPosition().y + (exit_background.getLocalBounds().height - exit_btn.getLocalBounds().height) / 4);
	/* ------------------------------------------- */

	/******************** ANOTHER OPTIONS ********************/
	static sf::RectangleShape an_option_bg(sf::Vector2f(win_width / 2 - 40 * d_scale, win_height / 3 + 30 * d_scale));
	an_option_bg.setFillColor(background_color_grey);
	an_option_bg.setPosition(win_width / 4 + 20 * d_scale, win_height - win_height / 3 - 100 * d_scale);

	static sf::Text capturing;
	capturing.setString(CAPTURING);
	capturing.setFont(font);
	capturing.setCharacterSize(20 * d_scale);
	capturing.setFillColor(text_color);
	capturing.setPosition(win_width / 4 + 50 * d_scale, an_option_bg.getPosition().y + 20 * d_scale);

	static sf::Sprite m_checkbox_1;
	m_checkbox_1.setTexture(m_checkbox_checked);
	m_checkbox_1.setPosition(win_width / 4 + 300 * d_scale, an_option_bg.getPosition().y + 20 * d_scale);
	m_checkbox_1.setScale(0.15 * d_scale, 0.15 * d_scale);

	static sf::Text f_double_three;
	f_double_three.setString(DOUBLE_THREE);
	f_double_three.setFont(font);
	f_double_three.setCharacterSize(20 * d_scale);
	f_double_three.setFillColor(text_color);
	f_double_three.setPosition(win_width / 4 + 50 * d_scale, win_height - win_height / 3 - 40 * d_scale);

	static sf::Sprite m_checkbox_2;
	m_checkbox_2.setTexture(m_checkbox_checked);
	m_checkbox_2.setPosition(win_width / 4 + 300 * d_scale, win_height - win_height / 3 - 40 * d_scale);
	m_checkbox_2.setScale(0.15 * d_scale, 0.15 * d_scale);

	static sf::Text only_five;
	only_five.setString(ONLY_FIVE_WINS);
	only_five.setFont(font);
	only_five.setCharacterSize(20 * d_scale);
	only_five.setFillColor(text_color);
	only_five.setPosition(win_width / 4 + 50 * d_scale, win_height - win_height / 3);

	static sf::Sprite m_checkbox_3;
	m_checkbox_3.setTexture(m_checkbox_unchecked);
	m_checkbox_3.setPosition(win_width / 4 + 300 * d_scale, win_height - win_height / 3);
	m_checkbox_3.setScale(0.15 * d_scale, 0.15 * d_scale);

	static sf::Text show_hint;
	show_hint.setString(SHOW_HINT);
	show_hint.setFont(font);
	show_hint.setCharacterSize(20 * d_scale);
	show_hint.setFillColor(text_color);
	show_hint.setPosition(win_width / 4 + 50 * d_scale, win_height - win_height / 3 + 80 * d_scale);

	static sf::Sprite m_checkbox_4;
	m_checkbox_4.setTexture(m_checkbox_checked);
	m_checkbox_4.setPosition(win_width / 4 + 300 * d_scale, win_height - win_height / 3 + 80 * d_scale);
	m_checkbox_4.setScale(0.15 * d_scale, 0.15 * d_scale);

	/******************** Volume btn ********************/
	static sf::Sprite volume_btn;
	if (music.getStatus() == sf::Music::Status::Playing)
		volume_btn.setTexture(volume_on);
	else
		volume_btn.setTexture(volume_off);
	volume_btn.setPosition(win_width - 60 * d_scale, win_height - 60 * d_scale);
	volume_btn.setScale(0.1 * d_scale, 0.1 * d_scale);
	/* ------------------------------------------- */

	window.clear();

	while (true)
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed ||
				(event.type == sf::Event::MouseButtonPressed &&
				 sf::Mouse::isButtonPressed(sf::Mouse::Left) &&
				 exit_background.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)))))
			{
				window.close();
				exit(0);
			}
			else if (event.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (start_background.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					_players[0].name = _players[0].human ? HUMAN : COMPUTER;
					_players[1].name = _players[1].human ? HUMAN : COMPUTER;
					return  ;
				}
				else if (checkbox_1.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (!_players[0].human)
					{
						checkbox_1.setTexture(checkbox_checked);
						checkbox_2.setTexture(checkbox_unchecked);
						_players[0].human = !_players[0].human;
					}
				}
				else if (checkbox_2.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (_players[0].human && _players[1].human)
					{
						checkbox_2.setTexture(checkbox_checked);
						checkbox_1.setTexture(checkbox_unchecked);
						_players[0].human = !_players[0].human;
					}
				}
				else if (checkbox_3.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (!_players[1].human)
					{
						checkbox_3.setTexture(checkbox_checked);
						checkbox_4.setTexture(checkbox_unchecked);
						_players[1].human = !_players[1].human;
					}
				}
				else if (checkbox_4.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (_players[1].human && _players[0].human)
					{
						checkbox_4.setTexture(checkbox_checked);
						checkbox_3.setTexture(checkbox_unchecked);
						_players[1].human = !_players[1].human;
					}
				}
				else if (m_checkbox_1.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					_rules.enable_capture = !_rules.enable_capture;
					if (_rules.enable_capture)
						m_checkbox_1.setTexture(m_checkbox_checked);
					else
						m_checkbox_1.setTexture(m_checkbox_unchecked);
				}
				else if (m_checkbox_2.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					_rules.forbid_double_threes = !_rules.forbid_double_threes;
					if (_rules.forbid_double_threes)
						m_checkbox_2.setTexture(m_checkbox_checked);
					else
						m_checkbox_2.setTexture(m_checkbox_unchecked);
				}
				else if (m_checkbox_3.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					_rules.only_five = !_rules.only_five;
					if (_rules.only_five)
						m_checkbox_3.setTexture(m_checkbox_checked);
					else
						m_checkbox_3.setTexture(m_checkbox_unchecked);
				}
				else if (m_checkbox_4.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					_rules.move_hint = !_rules.move_hint;
					if (_rules.move_hint)
						m_checkbox_4.setTexture(m_checkbox_checked);
					else
						m_checkbox_4.setTexture(m_checkbox_unchecked);
				}
				else if (volume_btn.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (music.getStatus() == sf::Music::Status::Playing)
					{
						music.pause();
						volume_btn.setTexture(volume_off);
					}
					else if (music.getStatus() == sf::Music::Status::Paused)
					{
						music.play();
						volume_btn.setTexture(volume_on);
					}
				}
			}
		}

		window.clear();
		window.draw(sp_bacground);

		window.draw(option);

		window.draw(p1_background);
		window.draw(player_1);
		window.draw(black_stone);
		window.draw(chose_human_1);
		window.draw(chose_computer_1);
		window.draw(checkbox_1);
		window.draw(checkbox_2);

		window.draw(p2_background);
		window.draw(player_2);
		window.draw(white_stone);
		window.draw(chose_human_2);
		window.draw(chose_computer_2);
		window.draw(checkbox_3);
		window.draw(checkbox_4);

		window.draw(an_option_bg);
		window.draw(capturing);
		window.draw(m_checkbox_1);
		window.draw(f_double_three);
		window.draw(m_checkbox_2);
		window.draw(only_five);
		window.draw(m_checkbox_3);

		window.draw(show_hint);
		window.draw(m_checkbox_4);

		window.draw(start_background);
		window.draw(start_btn);

		window.draw(exit_background);
		window.draw(exit_btn);

		window.draw(volume_btn);
		window.display();
	}
}

void Game::start_game(void)
{

	initialization();
	while (true)
	{
		show_menu();
		playing_loop();
	}
}

void Game::playing_loop(void)
{
	// clear board
	for (int y = 0; y < _board.size; ++y)
		for (int x = 0; x < _board.size; ++x)
			_board.horizontal[y][x] = FREE;


	logic->set_rules(_rules);

	/********************  Resize window ********************/
	int cell_size = cell.getSize().x * d_scale;
	int grid_size = cell_size * _board.size;

	win_width = grid_size + 400 * d_scale;
	win_height = grid_size + 100 * d_scale;

	if (desctop_height < win_height)
	{
		double scale = (double)(desctop_height - 100 * d_scale) / win_height;
		grid_size = cell.getSize().x * scale * _board.size * d_scale;

		cell_size = grid_size / _board.size;
		grid_size -= grid_size % _board.size;

		win_height = grid_size + 100 * d_scale;
		win_width = grid_size + 400 * d_scale;
	}

	window.setSize({win_width, win_height});
	window.setView(sf::View(sf::FloatRect(0, 0, win_width, win_height)));
	window.setPosition({(int)(desctop_wight / 2 - win_width / 2), (int)(desctop_height / 2 - win_height / 2)});
	/* ------------------------------------------- */

	sf::IntRect board(200 * d_scale, 25 * d_scale, grid_size, grid_size);

	/********************  Backdround  ********************/
	static sf::Sprite sp_background;
	sp_background.setTexture(bacground);
	sp_background.setPosition(0, 0);
	sp_background.setScale((double)win_width / bacground.getSize().x, (double)win_height / bacground.getSize().y);
	/* ------------------------------------------- */

	/********************  Exit btn  ********************/
	static sf::RectangleShape exit_background(sf::Vector2f(100 * d_scale, 40 * d_scale));
	exit_background.setFillColor(sf::Color(255, 0, 0, 170));
	exit_background.setPosition(win_width / 2 + 10 * d_scale, win_height - 60 * d_scale);

	static sf::Text exit_btn;
	exit_btn.setString(EXIT);
	exit_btn.setFont(font);
	exit_btn.setCharacterSize(24 * d_scale);
	exit_btn.setFillColor(sf::Color::White);
	exit_btn.setPosition(exit_background.getPosition().x + (exit_background.getLocalBounds().width - exit_btn.getLocalBounds().width) / 2,
						exit_background.getPosition().y + (exit_background.getLocalBounds().height - exit_btn.getLocalBounds().height) / 4);
	/* ------------------------------------------- */

	/********************  Return btn  ********************/
	static sf::RectangleShape return_background(sf::Vector2f(100 * d_scale, 40 * d_scale));
	return_background.setFillColor(background_color_green);
	return_background.setPosition(win_width / 2 - 110 * d_scale, win_height - 60 * d_scale);

	static sf::Text return_btn;
	return_btn.setString(RETURN);
	return_btn.setFont(font);
	return_btn.setCharacterSize(24 * d_scale);
	return_btn.setFillColor(sf::Color::White);
	return_btn.setPosition(return_background.getPosition().x + (return_background.getLocalBounds().width - return_btn.getLocalBounds().width) / 2,
						return_background.getPosition().y + (return_background.getLocalBounds().height - return_btn.getLocalBounds().height) / 4);
	/* ------------------------------------------- */

	/******************** Volume btn ********************/
	static sf::Sprite volume_btn;
	if (music.getStatus() == sf::Music::Status::Playing)
		volume_btn.setTexture(volume_on);
	else
		volume_btn.setTexture(volume_off);
	volume_btn.setPosition(win_width - 60 * d_scale, win_height - 60 * d_scale);
	volume_btn.setScale(0.1 * d_scale, 0.1 * d_scale);
	/* ------------------------------------------- */

	/******************** PLAYER 1 info ********************/
	sf::RectangleShape p1_background(sf::Vector2f(180 * d_scale, grid_size));
	p1_background.setFillColor(background_color_grey);
	p1_background.setPosition(10 * d_scale, 25 * d_scale);

	static sf::Text player_1;
	player_1.setString(PLAYER_1);
	player_1.setFont(font);
	player_1.setCharacterSize(24 * d_scale);
	player_1.setFillColor(text_color);
	player_1.setPosition(20 * d_scale, 35 * d_scale);

	static sf::Sprite black_stone;
	black_stone.setTexture(bs_texture);
	black_stone.setPosition(140 * d_scale, 35 * d_scale);
	black_stone.setScale(0.1 * d_scale, 0.1 * d_scale);

	static sf::Text player_1_name;
	player_1_name.setString(_players[0].name);
	player_1_name.setFont(font);
	player_1_name.setCharacterSize(24 * d_scale);
	player_1_name.setFillColor(text_color);
	player_1_name.setPosition(10 + (p1_background.getLocalBounds().width - player_1_name.getLocalBounds().width) / 2, 100 * d_scale);

	static sf::Text player_1_captured_text;
	player_1_captured_text.setString(CAPTURED);
	player_1_captured_text.setFont(font);
	player_1_captured_text.setCharacterSize(24 * d_scale);
	player_1_captured_text.setFillColor(text_color);
	player_1_captured_text.setPosition(20 * d_scale, 150 * d_scale);

	static sf::Text player_1_captured;
	player_1_captured.setString(std::to_string(_players[0].captured));
	player_1_captured.setFont(font);
	player_1_captured.setCharacterSize(24 * d_scale);
	player_1_captured.setFillColor(text_color);
	player_1_captured.setPosition(160 * d_scale - player_1_captured.getLocalBounds().width, 150 * d_scale);

	sf::Sprite player_1_watch;
	player_1_watch.setTexture(watch);
	player_1_watch.setScale(p1_background.getLocalBounds().width / player_1_watch.getLocalBounds().width,
							p1_background.getLocalBounds().width / player_1_watch.getLocalBounds().width);
	player_1_watch.setPosition(p1_background.getPosition().x, grid_size - player_1_watch.getLocalBounds().height - 25 * d_scale);

	static sf::Text player_1_time;
	player_1_time.setString(get_duration(std::chrono::nanoseconds(0)));
	player_1_time.setFont(time_font);
	player_1_time.setCharacterSize(12 * d_scale);
	player_1_time.setFillColor(sf::Color::Black);
	player_1_time.setPosition(player_1_watch.getPosition().x + (player_1_watch.getGlobalBounds().width - player_1_time.getGlobalBounds().width) / 2,
							  player_1_watch.getPosition().y + (player_1_watch.getGlobalBounds().height - player_1_time.getGlobalBounds().height) / 2);

	/* ------------------------------------------- */

	/******************** PLAYER 2 info ********************/
	sf::RectangleShape p2_background(sf::Vector2f(180 * d_scale, grid_size));
	p2_background.setFillColor(background_color_grey);
	p2_background.setPosition(win_width - 190 * d_scale, 25 * d_scale);

	static sf::Text player_2;
	player_2.setString(PLAYER_2);
	player_2.setFont(font);
	player_2.setCharacterSize(24 * d_scale);
	player_2.setFillColor(text_color);
	player_2.setPosition(win_width - 180 * d_scale, 35 * d_scale);

	static sf::Sprite white_stone;
	white_stone.setTexture(ws_texture);
	white_stone.setPosition(win_width - 60 * d_scale, 35 * d_scale);
	white_stone.setScale(0.1 * d_scale, 0.1 * d_scale);

	static sf::Text player_2_name;
	player_2_name.setString(_players[1].name);
	player_2_name.setFont(font);
	player_2_name.setCharacterSize(24 * d_scale);
	player_2_name.setFillColor(text_color);
	player_2_name.setPosition(win_width - 10 * d_scale - (p2_background.getLocalBounds().width - player_2_name.getLocalBounds().width) / 2 - player_2_name.getLocalBounds().width, 100 * d_scale);

	static sf::Text player_2_captured_text;
	player_2_captured_text.setString(CAPTURED);
	player_2_captured_text.setFont(font);
	player_2_captured_text.setCharacterSize(24 * d_scale);
	player_2_captured_text.setFillColor(text_color);
	player_2_captured_text.setPosition(win_width - 180 * d_scale, 150 * d_scale);

	static sf::Text player_2_captured;
	player_2_captured.setString(std::to_string(_players[1].captured));
	player_2_captured.setFont(font);
	player_2_captured.setCharacterSize(24 * d_scale);
	player_2_captured.setFillColor(text_color);
	player_2_captured.setPosition(win_width - 40 * d_scale - player_2_captured.getLocalBounds().width, 150 * d_scale);

	sf::Sprite player_2_watch;
	player_2_watch.setTexture(watch);
	player_2_watch.setScale(p2_background.getLocalBounds().width / player_2_watch.getLocalBounds().width,
							p2_background.getLocalBounds().width / player_2_watch.getLocalBounds().width);
	player_2_watch.setPosition(p2_background.getPosition().x, p2_background.getLocalBounds().height - player_2_watch.getLocalBounds().height - 25 * d_scale);

	static sf::Text player_2_time;
	player_2_time.setString(get_duration(std::chrono::nanoseconds(0)));
	player_2_time.setFont(time_font);
	player_2_time.setCharacterSize(12 * d_scale);
	player_2_time.setFillColor(sf::Color::Black);
	player_2_time.setPosition(player_2_watch.getPosition().x + (player_2_watch.getGlobalBounds().width - player_2_time.getGlobalBounds().width) / 2,
							  player_2_watch.getPosition().y + (player_2_watch.getGlobalBounds().height - player_2_time.getGlobalBounds().height) / 2);

	/* ------------------------------------------- */

	/******************** Result window ********************/

	sf::RectangleShape result_background(sf::Vector2f(win_width, win_height));
	result_background.setFillColor(background_color_grey);
	result_background.setPosition(0, 0);

	static sf::Text winner_player;
	winner_player.setFont(font);
	winner_player.setCharacterSize(30 * d_scale);
	winner_player.setFillColor(text_color);

	static sf::Text winner_text;
	winner_text.setString(WINNER_TEXT);
	winner_text.setFont(font);
	winner_text.setCharacterSize(30 * d_scale);
	winner_text.setFillColor(text_color);
	winner_text.setPosition((win_width - winner_text.getGlobalBounds().width) / 2,
							(win_height - winner_text.getGlobalBounds().height) / 2);
	/* ------------------------------------------- */

	bool	show_hint = false;

	sf::Sprite hint_stone;
	hint_stone.setTexture(hint_texture);
	hint_stone.setScale(cell_size / hint_stone.getGlobalBounds().width, cell_size / hint_stone.getGlobalBounds().height);


	sf::Sprite grid[_board.size * _board.size];
	initialize_grid(grid, _board.size, cell_size, 200 * d_scale, 25 * d_scale);
	window.clear();

	uint8_t whose_turn = 0;
	bool human_move_started = false;
	bool first_move = true;

	bool show_result = false;
	bool draw_game = false;

	auto start_time = std::chrono::steady_clock::now();
	auto end_time = std::chrono::steady_clock::now();

	std::map<std::pair<int, int>, sf::Sprite> all_stones;


	while (true)
	{
		if (!show_result && !logic->is_empty_space(_board))
		{
			show_result = true;
			draw_game = true;
		}

		if (!show_result && _players[whose_turn].human && !human_move_started)
		{
			if (_rules.move_hint)
			{
				t_point hint = {_board.size / 2, _board.size / 2};
				if (!first_move)
					hint = Logic::get_instance()->get_best_moves(_board, _players[whose_turn].figure, _players[whose_turn].captured, _players[!whose_turn].captured).move_cord;
				show_hint = true;
				hint_stone.setPosition((200 * d_scale + hint.x * cell_size) + (cell_size - hint_stone.getGlobalBounds().width) / 2,
									  (25 * d_scale + hint.y * cell_size) + (cell_size - hint_stone.getGlobalBounds().height) / 2);
			}

			if (first_move)
				first_move = false;

			human_move_started = true;
			(whose_turn == 0) ? p1_background.setFillColor(background_color_green) : p2_background.setFillColor(background_color_green);
			start_time = std::chrono::steady_clock::now();
		}
		else if (!show_result && !_players[whose_turn].human)
		{
			(whose_turn == 0) ? p1_background.setFillColor(background_color_green) :
													p2_background.setFillColor(background_color_green);

			t_move curr_move = {{_board.size / 2, _board.size / 2}, {}, {}};
			start_time = std::chrono::steady_clock::now();
			if (first_move)
				first_move = false;
			else
				curr_move = Logic::get_instance()->get_best_moves(_board, _players[whose_turn].figure, _players[whose_turn].captured, _players[!whose_turn].captured);
			end_time = std::chrono::steady_clock::now();
			_players[whose_turn].captured += curr_move.captured.size;

			std::chrono::nanoseconds time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
			(!whose_turn) ? player_1_time.setString(get_duration(time)) : player_2_time.setString(get_duration(time));
			(!whose_turn) ? p1_background.setFillColor(background_color_grey) : p2_background.setFillColor(background_color_grey);

			const int &x = curr_move.move_cord.x;
			const int &y = curr_move.move_cord.y;

			if (x == -1 || y == -1)
			{
				show_result = true;
				draw_game = true;
			}
			else
			{
				// change board
				_board.horizontal[y][x] = _players[whose_turn].figure;

				// put current figure
				sf::Sprite new_stone;
				new_stone.setTexture((_players[whose_turn].figure == X ? bs_texture : ws_texture));
				new_stone.setScale(cell_size / new_stone.getGlobalBounds().width, cell_size / new_stone.getGlobalBounds().height);
				new_stone.setPosition((200 * d_scale + x * cell_size) + (cell_size - new_stone.getGlobalBounds().width) / 2,
									  (25 * d_scale + y * cell_size) + (cell_size - new_stone.getGlobalBounds().height) / 2);

				all_stones[{x, y}] = new_stone;

				// check captured and remove them
				if (_rules.enable_capture && curr_move.captured.size != 0)
				{
					for (int index = 0; index < curr_move.captured.size; ++index)
					{
						auto elem = all_stones.find({curr_move.captured.arr[index].x, curr_move.captured.arr[index].y});
						if (elem != all_stones.end())
							all_stones.erase(elem);

						_board.horizontal[curr_move.captured.arr[index].y][curr_move.captured.arr[index].x] = FREE;
					}
				}

				if ((_rules.enable_capture && _players[whose_turn].captured >= 10) ||
					curr_move.heuristic.five > 0)
					show_result = true;
				else
					whose_turn = !whose_turn;
			}
		}

		sf::Event game_event;
		while (window.pollEvent(game_event))
		{
			if (game_event.type == sf::Event::Closed ||
				(game_event.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left) && exit_background.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)))))
			{
				window.close();
				exit(0);
			}
			else if (game_event.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (return_background.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
					return ;
				else if (!show_result && volume_btn.getGlobalBounds().contains(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window))))
				{
					if (music.getStatus() == sf::Music::Status::Playing)
					{
						music.pause();
						volume_btn.setTexture(volume_off);
					}
					else if (music.getStatus() == sf::Music::Status::Paused)
					{
						music.play();
						volume_btn.setTexture(volume_on);
					}
				}
				else if (!show_result && _players[whose_turn].human && board.contains(static_cast<sf::Vector2i>(sf::Mouse::getPosition(window))))
				{
					const int x = (sf::Mouse::getPosition(window).x - 200 * d_scale) / cell_size;
					const int y = (sf::Mouse::getPosition(window).y - 25 * d_scale) / cell_size;

					if (_board.horizontal[y][x] == FREE)
					{
						t_move curr_move;
						curr_move = {{x, y}, {}, {}};
						if (logic->is_move_allowed(_board, curr_move, _players[whose_turn].figure))
						{
							end_time = std::chrono::steady_clock::now();
							// stop and chane time
							human_move_started = false;
							std::chrono::nanoseconds time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
							(!whose_turn) ? player_1_time.setString(get_duration(time)) : player_2_time.setString(get_duration(time));
							(!whose_turn) ? p1_background.setFillColor(background_color_grey) : p2_background.setFillColor(background_color_grey);

							// change board
							_board.horizontal[y][x] = _players[whose_turn].figure;

							// put current figure
							sf::Sprite new_stone;
							new_stone.setTexture((_players[whose_turn].figure == X ? bs_texture : ws_texture));
							new_stone.setScale(cell_size / new_stone.getGlobalBounds().width, cell_size / new_stone.getGlobalBounds().height);
							new_stone.setPosition((200 * d_scale + x * cell_size) + (cell_size - new_stone.getGlobalBounds().width) / 2,
												  (25 * d_scale + y * cell_size) + (cell_size - new_stone.getGlobalBounds().height) / 2);

							all_stones[{x, y}] = new_stone;

							// check captured and remove them
							if (_rules.enable_capture && curr_move.captured.size != 0)
							{
								for (int index = 0; index < curr_move.captured.size; ++index)
								{
									auto elem = all_stones.find({curr_move.captured.arr[index].x, curr_move.captured.arr[index].y});
									if (elem != all_stones.end())
										all_stones.erase(elem);

									_board.horizontal[curr_move.captured.arr[index].y][curr_move.captured.arr[index].x] = FREE;
									_players[whose_turn].captured++;
								}
							}

							if ((_rules.enable_capture && _players[whose_turn].captured >= 10) ||
								curr_move.heuristic.five > 0)
								show_result = true;
							else
								whose_turn = !whose_turn;
							show_hint = false;
						}
					}
				}
			}
		}

		window.clear();

		window.draw(sp_background);
		window.draw(volume_btn);

		window.draw(p1_background);
		window.draw(player_1);
		window.draw(black_stone);
		window.draw(player_1_name);
		if (_rules.enable_capture)
		{
			player_1_captured.setString(std::to_string(_players[0].captured));
			window.draw(player_1_captured_text);
			window.draw(player_1_captured);
		}
		window.draw(player_1_watch);
		window.draw(player_1_time);

		window.draw(p2_background);
		window.draw(player_2);
		window.draw(white_stone);
		window.draw(player_2_name);
		if (_rules.enable_capture)
		{
			player_2_captured.setString(std::to_string(_players[1].captured));
			window.draw(player_2_captured_text);
			window.draw(player_2_captured);
		}
		window.draw(player_2_watch);
		window.draw(player_2_time);

		draw_board(grid, _board.size);
		for (auto &stone : all_stones)
			window.draw(stone.second);

		if (show_result)
		{
			if (draw_game)
				winner_player.setString(DRAW_GAME);
			else
				winner_player.setString(whose_turn == 0 ? PLAYER_1 : PLAYER_2);

			winner_player.setPosition((win_width - winner_player.getGlobalBounds().width) / 2,
									(win_height - winner_player.getGlobalBounds().height) / 2 + 40 * d_scale);
			window.draw(result_background);
			window.draw(winner_player);
			if (!draw_game)
				window.draw(winner_text);
		}

		if (show_hint)
			window.draw(hint_stone);

		window.draw(exit_background);
		window.draw(exit_btn);
		window.draw(return_background);
		window.draw(return_btn);

		window.display();
	}
}
