#include "Game.hpp"

static const auto _ = [](){
	std::ios::sync_with_stdio(false);
	std::cin.tie(nullptr);
	return nullptr;
}();

int main()
{
	Game game;
	game.start_game();

	return (0);
}
