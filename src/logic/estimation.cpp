#include "Logic.hpp"

void	Logic::find_sequnces(const t_state *board, const t_state &figure, t_heuristic &out) const
{
	int	consecutive = 0;
	int	open_before = 0;

	out = {};

	for (int cell = 0; board[cell] != SYS_INFO && cell < MAX_SIZE; ++cell)
	{
		if (board[cell] == figure) {
			++consecutive;

		} else if (board[cell] == FREE && consecutive > 0) {
			int open_after = 1;
			int index = cell + 1;

			while(index < MAX_SIZE && (board[index] == FREE || board[index] == figure))
			{
				index++;
				open_after++;
			}

			// specific case for full opened three sequence
			if (open_before != 0 && consecutive == 2 && cell + 2 < MAX_SIZE &&
				board[cell + 1] == figure &&
				board[cell + 2] == FREE) {
				// 0 X X 0 X 0
				estimate_sequence(3, open_before, open_after, out);
				cell += 2;

			} else if (open_before != 0 && consecutive == 1 && cell + 3 < MAX_SIZE &&
				board[cell + 1] == figure &&
				board[cell + 2] == figure &&
				board[cell + 3] == FREE) {
				// 0 X 0 X X 0
				estimate_sequence(3, open_before, open_after, out);
				cell += 3;

			} else {
				estimate_sequence(consecutive, open_before, open_after, out);
			}

			consecutive = 0;
			open_before = 1;

		} else if (board[cell] == FREE) {
			open_before++;

		} else if (consecutive > 0) {
			estimate_sequence(consecutive, open_before, 0, out);
			consecutive = 0;
			open_before = 0;

		} else {
			open_before = 0;
		}
	}

	if (consecutive > 0)
		estimate_sequence(consecutive, open_before, 0, out);
}


void	Logic::estimate_sequence(const int &sequence, const int &ends_before, const int &ends_after, t_heuristic &heur_out) const {

	if (sequence > 5 && _rules.only_five)
		return ;

	if ((ends_before + ends_after + sequence) < 5)
		return ;

	if (sequence >= 5) {
		++heur_out.five;
	} else if (sequence == 4) {
		if (ends_before == 0 || ends_after == 0)
			++heur_out.open_four;
		else
			++heur_out.full_open_four;

	} else if (sequence == 3) {
		if (ends_before == 0 || ends_after == 0)
			++heur_out.open_three;
		else
			++heur_out.full_open_three;

	} else if (sequence == 2) {
		if (ends_before == 0 || ends_after == 0)
			++heur_out.open_two;
		else
			++heur_out.full_open_two;

	} else if (sequence == 1) {
		if (ends_before == 0 || ends_after == 0)
			++heur_out.open_one;
		else
			++heur_out.full_open_one;
	} else
		LOG(ERROR, "undefined sequence")
}

void	Logic::sumarize_score(t_heuristic &out) const {
	if (out.five > 0)
		out.all_sum += 200000000;

	out.all_sum += out.full_open_four * 10000000;
	out.all_sum += out.open_four * 500000;

	out.all_sum += out.full_open_three * 100000;
	out.all_sum += out.open_three * 50000;

	out.all_sum += out.full_open_two * 10000;
	out.all_sum += out.open_two * 5000;

	out.all_sum += out.full_open_one * 100;
	out.all_sum += out.open_one * 50;

	// capturing score
	out.all_sum += out.n_capture * 50000;
}