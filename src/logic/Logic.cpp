#include "Logic.hpp"

#include <limits>

Logic* Logic::_instance = nullptr;


Logic*	Logic::get_instance(void) {
	if (_instance == nullptr)
		_instance = new Logic();

	return _instance;
}


Logic::Logic() : _rules({false, false, false, false}) { }


void	Logic::set_rules(t_rules rules) {
	_rules = rules;
}


bool	Logic::is_empty_space(const t_board &board) const {
	bool res = false;
	const int &size = board.size;

	for (int y = 0; y < size; ++y)
	{
		for (int x = 0; x < size; ++x)
		{
			if (board.horizontal[y][x] == FREE)
			{
				res = true;
				break ;
			}
		}
	}

	return res;
}


bool	Logic::is_move_allowed(const t_board &board, t_move &move, const t_state &figure) const {
	LOG(INFO, "check that move allowed")

	const int &size = board.size;

	if (size == 0)
	{
		LOG(WARNING, "game board has zero size")
		return false;
	}

	if (figure != X && figure != O)
	{
		LOG(ERROR, "brocken figure")
		return false;
	}

	const int &x = move.move_cord.x;
	const int &y = move.move_cord.y;

	if (x > size || x < 0 ||
		y > size || y < 0)
	{
		LOG(WARNING, "move beyond the board")
		return false;
	}

	if (board.horizontal[y][x] != t_state::FREE)
	{
		LOG(WARNING, "tile already occupied")
		return false;
	}

	if (_rules.enable_capture && is_forbiden_by_capturing(board, figure, move))
	{
		LOG(WARNING, "move forbiden by capturing")
		return false;
	}

	// initialize views
	t_board_heur inf = {board, {}};
	initialize_views(inf);

	get_all_view_heuristic(inf, figure, 0);
	t_heuristic initial = inf.heu[static_cast<uint8_t>(figure)].all;

	// make move and count capturin and heuristic
	make_move(inf, figure, move);

	if (_rules.forbid_double_threes &&
		(move.heuristic.full_open_three - initial.full_open_three) >= 2)
	{
		LOG(WARNING, "move forbiden by double-three rule")
		return false;
	}

	LOG(INFO, "move allowed")
	return true;
}


t_move	Logic::get_best_moves(const t_board &board, const t_state &figure, const int cap_curr, const int cap_opon) const {
	if (board.size <= 0 || board.size > MAX_SIZE ||
		(figure != X && figure != O) || !is_empty_space(board))
	{
		LOG(INFO, "somthing wrong")
		return {{-1, -1}, {}, {}};
	}

	t_board_heur b_inf = {board, {}};
	initialize_views(b_inf);

	// count heuristic for both players
	get_all_view_heuristic(b_inf, figure, cap_curr);


	t_min_max_inf mm_inf = {{0, 0}, 5, true, std::numeric_limits<long long>::min(), std::numeric_limits<long long>::max()};
	mm_inf.cap[static_cast<uint8_t>(figure)] = cap_curr;
	mm_inf.cap[static_cast<uint8_t>(!figure)] = cap_opon;

	t_move res = min_max_alg(mm_inf, b_inf, figure);
	LOG(INFO, "Chosen move x= " << res.move_cord.x << "; y = " << res.move_cord.y << " heur " << res.heuristic.all_sum)
    return res;
}