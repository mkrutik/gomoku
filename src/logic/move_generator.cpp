#include "Logic.hpp"

void	Logic::generate_moves(const t_board &board, t_move_array  &out) const {
	const int &size = board.size;

	out.size = 0;
	for (int y = 0; y < size; ++y) {
		for (int x =0; x < size; ++x) {

			if (board.horizontal[y][x]== FREE) {

				// by horizontal
				if (((x + 1) < size && board.horizontal[y][x + 1] != FREE) ||
					((x + 2) < size && board.horizontal[y][x + 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				else if (((x - 1) >= 0 && board.horizontal[y][x - 1] != FREE) ||
						 ((x - 2) >= 0 && board.horizontal[y][x - 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				// by vertical
				else if (((y + 1) < size && board.horizontal[y + 1][x] != FREE) ||
						 ((y + 2) < size && board.horizontal[y + 2][x] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				else if (((y - 1) >= 0 && board.horizontal[y - 1][x] != FREE) ||
						 ((y - 2) >= 0 && board.horizontal[y - 2][x] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				// left up to down right
				else if (((x + 1) < size && (y + 1) < size && board.horizontal[y + 1][x + 1] != FREE) ||
						 ((x + 2) < size && (y + 2) < size && board.horizontal[y + 2][x + 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				else if (((x - 1) >= 0 && (y - 1) >= 0 && board.horizontal[y - 1][x - 1] != FREE) ||
						 ((x - 2) >= 0 && (y - 2) >= 0 && board.horizontal[y - 2][x - 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				// right up to down left
				else if (((x - 1) >= 0 && (y + 1) < size && board.horizontal[y + 1][x - 1] != FREE) ||
						 ((x - 2) >= 0 && (y + 2) < size && board.horizontal[y + 2][x - 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};

				else if (((x + 1) < size && (y - 1) >= 0 && board.horizontal[y - 1][x + 1] != FREE) ||
						 ((x + 2) < size && (y - 2) >= 0 && board.horizontal[y - 2][x + 2] != FREE))
					out.arr[out.size++] = {{x, y}, {}, {}};
			}
		}
	}
}