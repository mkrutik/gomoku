#include "Logic.hpp"

#include <limits>

std::ostream& operator << (std::ostream &out, const t_heuristic &heu)
{
	out << "sequences : " << std::endl <<
			"five              : " << heu.five << std::endl <<
			"full opened four  : " << heu.full_open_four << std::endl <<
			"opened four       : " << heu.open_four << std::endl <<
			"full opened three : " << heu.full_open_three << std::endl <<
			"opened three      : " << heu.open_three << std::endl <<
			"full opened two   : " << heu.full_open_two << std::endl <<
			"opened two        : " << heu.open_two << std::endl <<
			"full opened one   : " << heu.full_open_one << std::endl <<
			"opened one        : " << heu.open_one << std::endl;
	return out;
}

int		insert_into_best_moves(t_best_moves &best_moves, size_t pos, t_move &move, t_board_heur &heur)
{
	if (best_moves.size > BEST_MOVES_AM)
	{
		LOG(ERROR, "Error: cannot insert into best moves: best_moves.size >= BEST_MOVES_AM")
		return (1);
	}
	if (pos >= BEST_MOVES_AM)
	{
		LOG(ERROR, "Error: cannot insert into best moves: pos >= BEST_MOVES_AM")
		return (1);
	}
	for (size_t i = BEST_MOVES_AM - 1; i > pos; i--)
	{
		best_moves.moves[i] = best_moves.moves[i - 1];
		best_moves.boards[i] = best_moves.boards[i - 1];
	}
	best_moves.moves[pos] = move;
	best_moves.boards[pos] = heur;
	if (best_moves.size < BEST_MOVES_AM)
		best_moves.size++;
	return (0);
}

int		Logic::select_best_moves(t_move_array &moves, t_best_moves &best_moves, t_state figure, bool maximizing, t_board_heur b_inf) const
{	t_state		I;
	t_state		enemy;

	I = figure;
	if (I == X)
		enemy = O;
	else
		enemy = X;
	if (!maximizing)
		std::swap(I, enemy);
	if (moves.size < 0)
		return (1);
	best_moves.size = 0;
	for (size_t i = 0; i < (size_t)moves.size; i++)
	{
		size_t			j;
		t_board_heur	tmp_b;

		tmp_b = b_inf;
		this->make_move(tmp_b, figure, moves.arr[i]);

		if (_rules.enable_capture && is_forbiden_by_capturing(b_inf.board, figure, moves.arr[i]))
		{
			LOG(INFO, "3) ! skip move due to capturing")
			continue ;
		}
		if (_rules.forbid_double_threes && (tmp_b.heu[static_cast<uint8_t>(figure)].all.full_open_three
			- b_inf.heu[static_cast<uint8_t>(figure)].all.full_open_three) >= 2)
		{
			LOG(INFO, "5) skip move due to double three")
			continue ;
		}

		moves.arr[i].heuristic.all_sum = tmp_b.heu[static_cast<uint8_t>(I)].all.all_sum -
									 tmp_b.heu[static_cast<uint8_t>(enemy)].all.all_sum;
		for (j = 0; j < best_moves.size; j++)
		{
			if ((best_moves.moves[j].heuristic.all_sum < moves.arr[i].heuristic.all_sum) == maximizing)
			{
				insert_into_best_moves(best_moves, j, moves.arr[i], tmp_b);
				break;
			}
		}
		if (j >= BEST_MOVES_AM)
			continue;
		if (j == best_moves.size)
			insert_into_best_moves(best_moves, j, moves.arr[i], tmp_b);
	}
	return (0);
}

t_move	Logic::min_max_alg(t_min_max_inf mm_inf, t_board_heur b_inf, const t_state figure) const {
	LOG(INFO, "** " << mm_inf.depth << " *************************************************************")

	t_move_array raw_moves = {};
	generate_moves(b_inf.board, raw_moves);

	LOG(INFO, "1) Genereted " << raw_moves.size << " moves")

	t_move res = {{-1, -1}, {}, {}};
	res.heuristic.all_sum = (mm_inf.maximizing) ? std::numeric_limits<long long>::min() :
												  std::numeric_limits<long long>::max();
	t_best_moves best_moves;
	if (this->select_best_moves(raw_moves, best_moves, figure, mm_inf.maximizing, b_inf))
	{
		LOG(INFO, "select_best_moves failed!")
		return (res);
	}

	for (size_t i = 0; i < best_moves.size; ++i)
	{
		if (best_moves.boards[i].heu[0].all.five != 0 || best_moves.boards[i].heu[1].all.five != 0 ||
			(best_moves.moves[i].captured.size + mm_inf.cap[static_cast<uint8_t>(figure)]) >= 10)
		{
			res = best_moves.moves[i];
			LOG(INFO, "6) FOUND game over")
			break ;
		}
		if (mm_inf.depth > 0) {
			t_min_max_inf tmp_mm = mm_inf;

			LOG(INFO, "7) recursion x = " << best_moves.moves[i].move_cord.x << " y = " << best_moves.moves[i].move_cord.y  << " maximizing: " << mm_inf.maximizing << " heu: " << best_moves.moves[i].heuristic.all_sum)

			tmp_mm.cap[static_cast<uint8_t>(figure)] += best_moves.moves[i].captured.size;
			tmp_mm.depth--;
			tmp_mm.maximizing = !mm_inf.maximizing;

			t_move newxt_move = min_max_alg(tmp_mm, best_moves.boards[i], static_cast<t_state>(!figure));

			LOG(INFO, "8) received heur " << newxt_move.heuristic.all_sum << " curr: x = " << res.move_cord.x << " y = " << res.move_cord.y << " heu: " << res.heuristic.all_sum)

			if (newxt_move.move_cord.x != -1)
				best_moves.moves[i].heuristic.all_sum = newxt_move.heuristic.all_sum;
		}
		if ((res.heuristic.all_sum < best_moves.moves[i].heuristic.all_sum) == mm_inf.maximizing)
		{
			LOG(INFO, "10 ) swap move, maximizing - " << mm_inf.maximizing << ":" << std::endl <<
					"curr: x = " << res.move_cord.x << " y = " << res.move_cord.y << " heu: " << res.heuristic.all_sum << std::endl <<
					"new: x = " << best_moves.moves[i].move_cord.x << " y = " << best_moves.moves[i].move_cord.y << " heu: " << best_moves.moves[i].heuristic.all_sum)
			res = best_moves.moves[i];
		}

		if (mm_inf.maximizing)
			mm_inf.alpha = std::max(mm_inf.alpha, res.heuristic.all_sum);
		else
			mm_inf.beta = std::min(mm_inf.beta, res.heuristic.all_sum);

		if (mm_inf.beta <= mm_inf.alpha)
		{
			LOG(INFO, "13) alpha greater then beta")
			break ;
		}
	}

	LOG(INFO, "14) Chosen move x = " << res.move_cord.x << "; y = " << res.move_cord.y << " heur " << res.heuristic.all_sum)
	return res;
}
