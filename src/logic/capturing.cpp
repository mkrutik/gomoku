#include "Logic.hpp"

bool    Logic::is_forbiden_by_capturing(const t_board &board, const t_state &figure, const t_move &move) const {
	const int &size = board.size;
	const int &x = move.move_cord.x;
	const int &y = move.move_cord.y;

	bool res = false;
	/*
		- X - -
		- nO - -
		- O - -
		- X - -
	*/
	if (y >= 1 && (y + 2) < size &&
		board.horizontal[y + 1][x] == figure &&
		board.horizontal[y - 1][x] != t_state::FREE && board.horizontal[y - 1][x] != figure &&
		board.horizontal[y + 2][x] != t_state::FREE && board.horizontal[y + 2][x] != figure)
			res = true;

	/*
		- X - -
		- O - -
		- nO - -
		- X - -
	*/
	if (!res && y >= 2 && (y + 1) < size &&
		board.horizontal[y - 1][x] == figure &&
		board.horizontal[y + 1][x] != t_state::FREE && board.horizontal[y + 1][x] != figure &&
		board.horizontal[y - 2][x] != t_state::FREE && board.horizontal[y - 2][x] != figure)
			res = true;


	//	X nO O X
	if (!res && x >= 1 && (x + 2) < size &&
		board.horizontal[y][x + 1] == figure &&
		board.horizontal[y][x - 1] != t_state::FREE && board.horizontal[y][x - 1] != figure &&
		board.horizontal[y][x + 2] != t_state::FREE && board.horizontal[y][x + 2] != figure)
			res = true;


	//	X O nO X
	if (!res && x >= 2 && (x + 1) < size &&
		board.horizontal[y][x - 1] == figure &&
		board.horizontal[y][x - 2] != t_state::FREE && board.horizontal[y][x - 2] != figure &&
		board.horizontal[y][x + 1] != t_state::FREE && board.horizontal[y][x + 1] != figure)
			res = true;

	/*
		X - - -
		- O - -
		- - nO -
		- - - X
	*/
	if (!res && y >= 2 && (y + 1) < size &&
		x >= 2 && (x + 1) < size &&
		board.horizontal[y - 1][x - 1] == figure &&
		board.horizontal[y + 1][x + 1] != t_state::FREE && board.horizontal[y + 1][x + 1] != figure &&
		board.horizontal[y - 2][x - 2] != t_state::FREE && board.horizontal[y - 2][x - 2] != figure)
			res = true;

	/*
		X - - -
		- nO - -
		- - O- -
		- - - X
	*/
	if (!res && y >= 1 && (y + 2) < size &&
		x >= 1 && (x + 2) < size &&
		board.horizontal[y + 1][x + 1] == figure &&
		board.horizontal[y - 1][x - 1] != t_state::FREE && board.horizontal[y - 1][x - 1] != figure &&
		board.horizontal[y + 2][x + 2] != t_state::FREE && board.horizontal[y + 2][x + 2] != figure)
			res = true;

	/*
		- - - X
		- - O -
		- nO - -
		X - - -
	*/
	if (!res && y >= 2 && (y + 1) < size &&
		x >= 1 && (x + 2) < size &&
		board.horizontal[y - 1][x + 1] == figure &&
		board.horizontal[y + 1][x - 1] != t_state::FREE && board.horizontal[y + 1][x - 1] != figure &&
		board.horizontal[y - 2][x + 2] != t_state::FREE && board.horizontal[y - 2][x + 2] != figure)
			res = true;

	/*
		- - - X
		- - nO -
		- O - -
		X - - -
	*/
	if (!res && y >= 1 && (y + 2) < size &&
		x >= 2 && (x + 1) < size &&
		board.horizontal[y + 1][x - 1] == figure &&
		board.horizontal[y - 1][x + 1] != t_state::FREE && board.horizontal[y - 1][x + 1] != figure &&
		board.horizontal[y + 2][x - 2] != t_state::FREE && board.horizontal[y + 2][x - 2] != figure)
			res = true;

	return res;
}


void	Logic::check_capturing(const t_board &board, const t_state &figure, t_move &move_out) const {
	const int &x = move_out.move_cord.x;
	const int &y = move_out.move_cord.y;
	const int &size = board.size;

	move_out.captured.size = 0;

	if (x < 0 || x >= size ||
		y < 0 || y >= size)
    {
		LOG(WARNING, "move beyond the board")
		return ;
	}

	/*
		- nO - -
		- X - -
		- X - -
		- O - -
	*/
	if ((y + 3) < size &&
		board.horizontal[y + 3][x]  == figure &&
		board.horizontal[y + 1][x] != t_state::FREE && board.horizontal[y + 1][x] != figure &&
		board.horizontal[y + 2][x] != t_state::FREE && board.horizontal[y + 2][x] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x, y + 1};
		move_out.captured.arr[move_out.captured.size++] = {x, y + 2};
		LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
    }

	/*
		- O - -
		- X - -
		- X - -
		- nO - -
	*/
	if ((y - 3) >= 0 &&
		board.horizontal[y - 3][x] == figure &&
		board.horizontal[y - 1][x] != t_state::FREE && board.horizontal[y - 1][x] != figure &&
		board.horizontal[y - 2][x] != t_state::FREE && board.horizontal[y - 2][x] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x, y - 1};
		move_out.captured.arr[move_out.captured.size++] = {x, y - 2};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	// nO X X O
	if ((x + 3) < size &&
		board.horizontal[y][x + 3] == figure &&
		board.horizontal[y][x + 1] != t_state::FREE && board.horizontal[y][x + 1] != figure &&
		board.horizontal[y][x + 2] != t_state::FREE && board.horizontal[y][x + 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x + 1, y};
		move_out.captured.arr[move_out.captured.size++] = {x + 2, y};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	// O X X nO
	if ((x - 3) >= 0 &&
		board.horizontal[y][x - 3] == figure &&
		board.horizontal[y][x - 1] != t_state::FREE && board.horizontal[y][x - 1] != figure &&
		board.horizontal[y][x - 2] != t_state::FREE && board.horizontal[y][x - 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x - 1, y};
		move_out.captured.arr[move_out.captured.size++] = {x - 2, y};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	/*
		nO - - -
		- X - -
		- - X -
		- - - O
	*/
	if ((x + 3) < size && (y + 3) < size &&
		board.horizontal[y + 3][x + 3] == figure &&
		board.horizontal[y + 1][x + 1] != t_state::FREE && board.horizontal[y + 1][x + 1] != figure &&
		board.horizontal[y + 2][x + 2] != t_state::FREE && board.horizontal[y + 2][x + 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x + 1, y + 1};
		move_out.captured.arr[move_out.captured.size++] = {x + 2, y + 2};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	/*
		O - - -
		- X - -
		- - X -
		- - - nO
	*/
	if ((x - 3) >= 0 && (y - 3) >= 0 &&
		board.horizontal[y - 3][x - 3] == figure &&
		board.horizontal[y - 1][x - 1] != t_state::FREE && board.horizontal[y - 1][x - 1] != figure &&
		board.horizontal[y - 2][x - 2] != t_state::FREE && board.horizontal[y - 2][x - 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x - 1, y - 1};
		move_out.captured.arr[move_out.captured.size++] = {x - 2, y - 2};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	/*
		- - - nO
		- - X -
		- X - -
		O - - -
	*/
	if ((x - 3) >= 0 && (y + 3) < size &&
		board.horizontal[y + 3][x - 3] == figure &&
		board.horizontal[y + 1][x - 1] != t_state::FREE && board.horizontal[y + 1][x - 1] != figure &&
		board.horizontal[y + 2][x - 2] != t_state::FREE && board.horizontal[y + 2][x - 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x - 1, y + 1};
		move_out.captured.arr[move_out.captured.size++] = {x - 2, y + 2};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}

	/*
		- - - O
		- - X -
		- X - -
		nO - - -
	*/
	if ((x + 3) < size && (y - 3) >= 0 &&
		board.horizontal[y - 3][x + 3] == figure &&
		board.horizontal[y - 1][x + 1] != t_state::FREE && board.horizontal[y - 1][x + 1] != figure &&
		board.horizontal[y - 2][x + 2] != t_state::FREE && board.horizontal[y - 2][x + 2] != figure)
	{
		move_out.captured.arr[move_out.captured.size++] = {x + 1, y - 1};
		move_out.captured.arr[move_out.captured.size++] = {x + 2, y - 2};
        LOG(INFO, "captured figures (x: " + std::to_string(x) + "; y: " + std::to_string(y + 1) + ")(x: " +
											std::to_string(x) + "; y: " + std::to_string(y + 2) + ")")
	}
}
