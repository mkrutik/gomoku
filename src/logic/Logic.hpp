#ifndef LOGIC_HPP
#define LOGIC_HPP

#include "../../includes/header.h"

void print_all(const t_board_heur &board_inf);



class Logic {

public:
	static  Logic*	get_instance(void);
	void			set_rules(t_rules rules);

	bool			is_empty_space(const t_board &board) const;
	bool			is_move_allowed(const t_board &board, t_move &move, const t_state &figure) const;
	t_move			get_best_moves(const t_board &board, const t_state &figure, const int cap_curr, const int cap_opo) const;

private:
	Logic();


	/********************	move checkers	********************/
	void		check_capturing(const t_board &board, const t_state &figure, t_move &move_out) const;
	bool		is_forbiden_by_capturing(const t_board &board, const t_state &figure, const t_move &move) const;
	/*---------------------------------------------------------*/


	/********************	evaluation helper functions	********************/
	void		find_sequnces(const t_state *board, const t_state &figure, t_heuristic &out) const;
	void		estimate_sequence(const int &sequence, const int &b_ends, const int &a_ends, t_heuristic &heur_out) const;
	void		sumarize_score(t_heuristic &heur_out) const;


	void		make_move(t_board_heur &inf, const t_state &figure, t_move &out) const;
	int			select_best_moves(t_move_array &moves, t_best_moves &best_moves, t_state figure, bool maximizing, t_board_heur b_inf) const;
	void		get_all_view_heuristic(t_board_heur &inf, const t_state &figure, int captured) const;

	/********************	Generate views functions ********************/
	void		initialize_views(t_board_heur &board_inf) const;

	void		horisontal_generate(t_board_heur &board_inf) const;
	void		vertical_generate(t_board_heur &board_inf) const;
	void		lu_rd_generate(t_board_heur &board) const;
	void		ld_ru_generate(t_board_heur &board) const;
	/*--------------------------------------------------------------*/


	/********************	Put figure functions to views ********************/
	inline void 	horizontal_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const;
	inline void 	vertical_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const;
	inline void		lu_rd_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const;
	inline void		ld_ru_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const;
	/*--------------------------------------------------------------*/


	/********************	Get index functions for views ********************/
	inline void		get_horizontal_index(const t_board &board, const t_point &point, t_view_indexes &out) const;
	inline void		get_vertical_index(const t_board &board, const t_point &point, t_view_indexes &out) const;
	inline void		get_lu_rd_index(const t_board &board, const t_point &point, t_view_indexes &out) const;
	inline void		get_ld_ru_index(const t_board &board, const t_point &point, t_view_indexes &out) const;
	/*--------------------------------------------------------------*/


	/********************	all posible moves generator	********************/
	void		generate_moves(const t_board &board, t_move_array  &out) const;
	/*---------------------------------------------------------------------*/


	/********************	main decision algorithm	********************/
	t_move		min_max_alg(t_min_max_inf mm_inf, t_board_heur b_inf, const t_state figure) const;
	/*---------------------------------------------------------------*/

private:

	static	Logic	*_instance;
	t_rules			_rules;
};

#endif