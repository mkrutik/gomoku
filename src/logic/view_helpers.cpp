#include "Logic.hpp"

#include <thread>
#include <mutex>

#ifdef DEBUG
void print_all(const t_board_heur &board_inf)
{

	for (int row = 0; row < board_inf.board.size; ++row)
	{
		for (int cell = 0; cell < board_inf.board.size; ++cell)
		{
			if (board_inf.board.horizontal[row][cell] == FREE)
				std::cout << "-";
			else if (board_inf.board.horizontal[row][cell] == X)
				std::cout << "X";
			else if (board_inf.board.horizontal[row][cell] == O)
				std::cout << "O";
			else
				std::cout << "!";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;


	for (int row = 0; row < board_inf.board.size; ++row)
	{
		for (int cell = 0; cell < board_inf.board.size; ++cell)
		{
			if (board_inf.board.vertical[row][cell] == FREE)
				std::cout << "-";
			else if (board_inf.board.vertical[row][cell] == X)
				std::cout << "X";
			else if (board_inf.board.vertical[row][cell] == O)
				std::cout << "O";
			else
				std::cout << "!";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;


	for (int row = 0; row < board_inf.board.size * 2 - 1; ++row)
	{
		for (int cell = 0; cell < board_inf.board.size && board_inf.board.ld_ru[row][cell] != SYS_INFO; ++cell)
		{
			if (board_inf.board.ld_ru[row][cell] == FREE)
				std::cout << "-";
			else if (board_inf.board.ld_ru[row][cell] == X)
				std::cout << "X";
			else if (board_inf.board.ld_ru[row][cell] == O)
				std::cout << "O";
			else
				std::cout << "!";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;


	for (int row = 0; row < board_inf.board.size * 2 - 1; ++row)
	{
		for (int cell = 0; cell < board_inf.board.size && board_inf.board.lu_rd[row][cell] != SYS_INFO; ++cell)
		{
			if (board_inf.board.lu_rd[row][cell] == FREE)
				std::cout << "-";
			else if (board_inf.board.lu_rd[row][cell] == X)
				std::cout << "X";
			else if (board_inf.board.lu_rd[row][cell] == O)
				std::cout << "O";
			else
				std::cout << "!";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
#endif

void	Logic::initialize_views(t_board_heur &board_inf) const {
	LOG(INFO, "create views")
	std::thread hor(&Logic::horisontal_generate, this, std::ref(board_inf));
	std::thread ver(&Logic::vertical_generate, this, std::ref(board_inf));
	std::thread lu_rd(&Logic::lu_rd_generate, this, std::ref(board_inf));
	std::thread ld_ru(&Logic::ld_ru_generate, this, std::ref(board_inf));

	hor.join();
	ver.join();
	lu_rd.join();
	ld_ru.join();
}

void	Logic::make_move(t_board_heur &inf, const t_state &figure, t_move &out) const
{
	LOG(INFO, "put figure to views")

	if (_rules.enable_capture) {
		check_capturing(inf.board, figure, out);

		for (int i = 0; i < out.captured.size; ++i)
		{
			t_view_indexes idx = {};
			get_horizontal_index(inf.board, out.captured.arr[i], idx);
			inf.board.horizontal[idx.row][idx.cell] = FREE;

			get_vertical_index(inf.board, out.captured.arr[i], idx);
			inf.board.vertical[idx.row][idx.cell] = FREE;

			get_ld_ru_index(inf.board, out.captured.arr[i], idx);
			inf.board.ld_ru[idx.row][idx.cell] = FREE;

			get_lu_rd_index(inf.board, out.captured.arr[i], idx);
			inf.board.lu_rd[idx.row][idx.cell] = FREE;
		}
	}

	horizontal_put_figure(inf, out.move_cord ,figure);
	vertical_put_figure(inf, out.move_cord ,figure);
	ld_ru_put_figure(inf, out.move_cord ,figure);
	lu_rd_put_figure(inf, out.move_cord ,figure);


	get_all_view_heuristic(inf, figure, out.captured.size);
	out.heuristic = inf.heu[static_cast<uint8_t>(figure)].all;
}

void	Logic::get_all_view_heuristic(t_board_heur &inf, const t_state &figure, int captured) const
{
	LOG(INFO, "count heuristic for all views")

	std::mutex mut;
	inf.heu[static_cast<uint8_t>(X)].all = {};
	inf.heu[static_cast<uint8_t>(O)].all = {};

	std::thread hor([&] () {
		for (int i = 0; i < inf.board.size; ++i) {
			{
				std::lock_guard<std::mutex> lk(mut);
				inf.heu[static_cast<uint8_t>(X)].all += inf.heu[static_cast<uint8_t>(X)].heu_horizontal[i];
				inf.heu[static_cast<uint8_t>(O)].all += inf.heu[static_cast<uint8_t>(O)].heu_horizontal[i];
			}
		}
	});

	std::thread ver([&] () {
		for (int i = 0; i < inf.board.size; ++i) {
			{
				std::lock_guard<std::mutex> lk(mut);
				inf.heu[static_cast<uint8_t>(X)].all += inf.heu[static_cast<uint8_t>(X)].heu_vertical[i];
				inf.heu[static_cast<uint8_t>(O)].all += inf.heu[static_cast<uint8_t>(O)].heu_vertical[i];
			}
		}
	});

	std::thread ld_ru([&] () {
		for (int i = 0; i < (inf.board.size * 2 - 1); ++i) {
			{
				std::lock_guard<std::mutex> lk(mut);
				inf.heu[static_cast<uint8_t>(X)].all += inf.heu[static_cast<uint8_t>(X)].heu_ld_ru[i];
				inf.heu[static_cast<uint8_t>(O)].all += inf.heu[static_cast<uint8_t>(O)].heu_ld_ru[i];
			}
		}
	});

	std::thread lu_rd([&] () {
		for (int i = 0; i < (inf.board.size * 2 - 1); ++i) {
			{
				std::lock_guard<std::mutex> lk(mut);
				inf.heu[static_cast<uint8_t>(X)].all += inf.heu[static_cast<uint8_t>(X)].heu_lu_rd[i];
				inf.heu[static_cast<uint8_t>(O)].all += inf.heu[static_cast<uint8_t>(O)].heu_lu_rd[i];
			}
		}
	});

	hor.join();
	ver.join();
	ld_ru.join();
	lu_rd.join();

	inf.heu[static_cast<uint8_t>(figure)].all.n_capture = captured;

	sumarize_score(inf.heu[static_cast<uint8_t>(X)].all);
	sumarize_score(inf.heu[static_cast<uint8_t>(O)].all);
}


/************************************	Generate views functions ************************************/

void	Logic::horisontal_generate(t_board_heur &inf) const {
	const int &size = inf.board.size;

	for (int i = 0; i < size; ++i)
	{
		find_sequnces(inf.board.horizontal[i], X, inf.heu[static_cast<uint8_t>(X)].heu_horizontal[i]);
		find_sequnces(inf.board.horizontal[i], O, inf.heu[static_cast<uint8_t>(O)].heu_horizontal[i]);
	}
}


void	Logic::vertical_generate(t_board_heur &inf) const {
	t_board &board = inf.board;
	const int &size = board.size;

	int row = 0;
	for (int x = 0; x < size; ++x) {

		int cell = 0;
		for (int y = 0; y < size; ++y) {
			board.vertical[row][cell] = board.horizontal[y][x];
			cell++;
		}

		find_sequnces(inf.board.vertical[row], X, inf.heu[static_cast<uint8_t>(X)].heu_vertical[row]);
		find_sequnces(inf.board.vertical[row], O, inf.heu[static_cast<uint8_t>(O)].heu_vertical[row]);

		row++;
	}
}

void	Logic::ld_ru_generate(t_board_heur &inf) const {
	t_board &board = inf.board;
	const int 	&size = board.size;

	int row = 0;
	for (int x = 0, y = 0; x < size && y < size; )
	{

		int cell = 0;
		for (int t_x = x, t_y = y; t_x < size && t_y >= 0; ++t_x, --t_y)
		{
			board.ld_ru[row][cell] = board.horizontal[t_y][t_x];
			cell++;
		}

		if (cell < MAX_SIZE)
			board.ld_ru[row][cell] = SYS_INFO;

		find_sequnces(inf.board.ld_ru[row], X, inf.heu[static_cast<uint8_t>(X)].heu_ld_ru[row]);
		find_sequnces(inf.board.ld_ru[row], O, inf.heu[static_cast<uint8_t>(O)].heu_ld_ru[row]);

		row++;

		if (y != (size - 1))
			y++;
		else
			x++;
	}

	if (row < MAX_VIEW_LINES)
		board.ld_ru[row][0] = SYS_INFO;
}

void	Logic::lu_rd_generate(t_board_heur &inf) const {
	t_board &board = inf.board;
	const int 	&size = board.size;

	int row = 0;
	for (int x = (size - 1), y = 0; x >= 0 && y < size; ) {

		int cell = 0;
		for (int t_x = x, t_y = y; t_x < size && t_y < size; ++t_x, ++t_y) {
			board.lu_rd[row][cell] = board.horizontal[t_y][t_x];
			cell++;
		}

		if (cell < MAX_SIZE)
			board.lu_rd[row][cell] = SYS_INFO;

		find_sequnces(inf.board.lu_rd[row], X, inf.heu[static_cast<uint8_t>(X)].heu_lu_rd[row]);
		find_sequnces(inf.board.lu_rd[row], O, inf.heu[static_cast<uint8_t>(O)].heu_lu_rd[row]);

		row++;

		if (x == 0)
			y++;
		else
			x--;
	}

	if (row < MAX_VIEW_LINES)
		board.lu_rd[row][0] = SYS_INFO;
}


/************************************	Put figure functions to views ************************************/
void 	Logic::horizontal_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const
{
	t_view_indexes index;
	get_horizontal_index(inf.board, point, index);
	inf.board.horizontal[index.row][index.cell] = figure;

	find_sequnces(inf.board.horizontal[index.row], X, inf.heu[static_cast<uint8_t>(X)].heu_horizontal[index.row]);
	find_sequnces(inf.board.horizontal[index.row], O, inf.heu[static_cast<uint8_t>(O)].heu_horizontal[index.row]);
}

void 	Logic::vertical_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const
{
	t_view_indexes index;
	get_vertical_index(inf.board, point, index);
	inf.board.vertical[index.row][index.cell] = figure;

	find_sequnces(inf.board.vertical[index.row], X, inf.heu[static_cast<uint8_t>(X)].heu_vertical[index.row]);
	find_sequnces(inf.board.vertical[index.row], O, inf.heu[static_cast<uint8_t>(O)].heu_vertical[index.row]);
}

void 	Logic::ld_ru_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const
{
	t_view_indexes index;
	get_ld_ru_index(inf.board, point, index);
	inf.board.ld_ru[index.row][index.cell] = figure;

	find_sequnces(inf.board.ld_ru[index.row], X, inf.heu[static_cast<uint8_t>(X)].heu_ld_ru[index.row]);
	find_sequnces(inf.board.ld_ru[index.row], O, inf.heu[static_cast<uint8_t>(O)].heu_ld_ru[index.row]);
}

void 	Logic::lu_rd_put_figure(t_board_heur &inf, const t_point &point, const t_state &figure) const
{
	t_view_indexes index;
	get_lu_rd_index(inf.board, point, index);
	inf.board.lu_rd[index.row][index.cell] = figure;

	find_sequnces(inf.board.lu_rd[index.row], X, inf.heu[static_cast<uint8_t>(X)].heu_lu_rd[index.row]);
	find_sequnces(inf.board.lu_rd[index.row], O, inf.heu[static_cast<uint8_t>(O)].heu_lu_rd[index.row]);
}




/************************************	Get index functions for views ************************************/
void	Logic::get_horizontal_index(const t_board &, const t_point &point, t_view_indexes &out) const
{
	out = {point.y, point.x};
}

void	Logic::get_vertical_index(const t_board &, const t_point &point, t_view_indexes &out) const
{
	out = {point.x, point.y};
}

void	Logic::get_lu_rd_index(const t_board &board, const t_point &point, t_view_indexes &out) const
{
	const int &size = board.size;
	int row = (size - 1) - point.x + point.y;
	int cell = (row < size) ? point.y : point.x;
	out = {row, cell};
}

void	Logic::get_ld_ru_index(const t_board &board, const t_point &point, t_view_indexes &out) const
{
	const int &size = board.size;
	int row = point.x + point.y;
	int cell;

	if (row > (size - 1)) {
		int line_diff = row - (size - 1);
		int line_size = size - line_diff;
		cell = (line_size - 1) - ((size - 1) - point.x);

	} else
		cell = point.x;

	out = {row, cell};
}